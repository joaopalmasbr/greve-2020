<div  id="boxConteudo">
	

		<div class="container" id="boxCartoes">

		<div class="row row-cols-1 row-cols-md-2">
		  <div class="col mb-3">
		    <div class="card">
		      <img src="./img/whatsapp.png" class="card-img-top" alt="...">
		      <div class="card-body">
		        <h5 class="card-title">Grupo WhatsApp</h5>
		        <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Ler mais...</button>
		      </div>
		    </div>
		  </div>
		  <div class="col mb-3">
		    <div class="card">
		      <img src="./img/regulamento.png" class="card-img-top" alt="...">
		      <div class="card-body">
		        <h5 class="card-title">Regulamento</h5>
		        <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Ler mais...</button>
		      </div>
		    </div>
		  </div>
		  <div class="col mb-4">
		    <div class="card">
		      <img src="./img/not.png" class="card-img-top" alt="...">
		      <div class="card-body">
		        <h5 class="card-title">Nome do Card</h5>
		        <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Ler mais...</button>
		      </div>
		    </div>
		  </div>
		  <div class="col mb-4">
		    <div class="card">
		      <img src="./img/not.png" class="card-img-top" alt="...">
		      <div class="card-body">
		        <h5 class="card-title">Nome do Card</h5>
		        <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Ler mais...</button>
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>

	<div id="contato">
		<form class="container text-white">
		<h1 class="text-white">Contato: </h1>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="inputEmail4">Nome:</label>
		      <input type="name" class="form-control" id="inputEmail4" placeholder="Digite seu nome">
		    </div>
		    <div class="form-group col-md-6">
		      <label for="inputEmail4">Email:</label>
		      <input type="email" class="form-control" id="inputEmail4" placeholder="Digite seu email">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="inputAddress">Assunto: </label>
		    <input type="text-area" class="form-control" id="" placeholder="Digite o assunto">
		  </div>

		  <div class="form-group">
		    <label for="exampleFormControlTextarea1">Mensagem: </label>
		    <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" placeholder="Digite sua mensagem..."></textarea>
		  </div>
		  
		  <div class="form-group">
		  </div>
		  <button type="submit" class="btn btn-light">Enviar</button>
		</form>
	</div>
	<div id="downloads">
		<div class="container">
			<h1>Downloads: </h1>
		<div class="row">			
			<div class="card" style="width: 18rem;" id="cardDownloads">
			  <img src="./img/logocard.png" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Logomarca Edição IX</h5>
			    <p class="card-text">Clique no botão abaixo para efetuar o download do arquivo em "png".</p>
			    <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Baixar</button>
			  </div>
			</div>
			<div class="card" style="width: 18rem;" id="cardDownloads">
			  <img src="./img/not.png" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Nome do arquivo</h5>
			    <p class="card-text">Descrição do arquivo.</p>
			    <button type="button" class="btn" style="background-color: #E71769; color: #fff;">Baixar</button>
			  </div>
			</div>
		</div>
		
	</div>
</div>
