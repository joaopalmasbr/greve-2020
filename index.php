<!doctype html>
<html lang="pt">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="./conteudo/images/favicon/ativo.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <!-- <link rel="stylesheet" href="./css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
        <script>sessionStorage.setItem('loginerrado','nao');</script>
        <style type="text/css">
            .topo{
                display: inline-block;
                width: 100%;
                height: 280px;
                background-color: #E71769;
                /*background-image: url('https://i.pinimg.com/originals/5a/6f/74/5a6f74db531abe5c5128ed8391c933a3.jpg');*/
                background-size: 100%; 
            }

            #bNav{
                margin-top:9px;
                z-index: 2; 

            }

            #logoIndex{ 
                height: 180px;
                margin-top:35px;
            }

            #bNav{
             border-radius: 15px 15px 0px 0px !important;
            }

            #conteudoPageHome{
                width: 80%;
                background-color: gray; 
            }

            #boxCartoes{
                margin-top: 50px !important;
                margin-bottom: 50px;
            }

            #footer{
                display: inline-block;
                width: 100%;
                height: 120px;
                background-color: #E71769;
            }

            #infoFooter{
                display: inline-block;
                width: 100%;
                height: 110px;
                background-color: #cf145e;
                margin-top:30px;
            }

            #infoFooter p{
                color: #fff;
                margin:25px;
            }

            #contato{
                display: inline-block;
                width: 100%;
                padding-top: 80px;
                padding-bottom: 80px;
                background-color: #E71769;
            }

            #downloads{
                display: inline-block;
                width: 100%;
                padding-top: 80px;
                padding-bottom: 80px;
                background-color: #fff;
            }

            #cardDownloads{
                margin: 35px;
            }
        </style>

        <title>GxB</title>
    </head>
    <body>
        <div class="topo">

            <div class="text-center">
                <img src="./extras/greve2020.png" id="logoIndex">
            </div>

            <div class="text-center">
            </div>
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light" id="bNav">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                      <li class="nav-item active">
                        <a class="nav-link" href="./">Início<span class="sr-only">(current)</span></a>
                      </li>
                      <li class="nav-item ">
                            <a class="nav-link" href="#contato">Contato</a>
                      </li>
                      <li class="nav-item ">
                            <a class="nav-link" href="#downloads">Downloads</a>
                      </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./login">Login</a>
                        </li>
                  </div>    
                </nav>
            </div>
        </div>
        <?php
            include("./connect.php");

            include("./conteudo.php");
            // include("conteudo.php");
        ?>

<div id="footer" class="text-center">
    <div id="infoFooter">
        <div class="container">
            <p>© 2020 Greve x Boleto - Todos os Direitos Reservados.</p>
        </div>     
    </div>    
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
