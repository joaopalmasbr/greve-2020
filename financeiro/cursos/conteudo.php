<div class="container">
    <a class="btn btn-info btn-block" id="btnAdd" href="./adicionar/">Adicionar</a>
    <br>
    <div class="table-responsive">
        <table class="table table-sm table-striped table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nome</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $query = "SELECT * FROM `curso`";
            if ($result = $mysqli->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    $id = $row['id'];
                    $nome = $row['nome'];
                    echo "<tr>
                                <th scope=\"row\">$id</th>                  
                                    <td>$nome</td>
                                    <td><a class=\"btn btn-sm btn-warning\" href=\"./editar/?id=$id\"><img src='../../img/edit.png' height='15'></a> 
                                    <a class=\"btn btn-sm btn-danger\" href=\"./deletar/?id=$id\"><img src='../../img/delete.png' height='15'></a></td>
                                </tr>";
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
