<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Cursos</a></li>
            <li class="breadcrumb-item active" saria-current="page">Adicionar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome da Curso" autofocus required>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Salvar</button>
        <br>
    </form>
</div>
