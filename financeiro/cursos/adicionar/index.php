<!doctype html>
<html lang="pt">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../../../conteudo/images/favicon/ativo.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../../../css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>GxB</title>
        <style>
            #sticky-footer {
                flex-shrink: none;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark justify-content-center">
            <a class="navbar-brand" href="../../../">
                <img src="../../../conteudo/images/icon/ativo.png" height="150px">
            </a>
        </nav>
        <nav class="navbar sticky-top navbar-dark bg-dark justify-content-center">
            <ul class="nav nav-pills justify-content-center">
                <li class="nav-item">
                    <a class="nav-link " href="../../">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../../atleticas">Atleticas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="../../modalidades">Modalidades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../../instituicoes">Instituições</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="../../cursos">Cursos</a>
                </li>
            </ul>
        </nav>
        <br>
        <?php
            try {
                include("../../../connect.php");
            } catch (Exception $e) {
                echo "<script>window.location.assign('../../../')</script>";
            }

            include("conteudo.php");
        ?>
        <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
            <div class="container text-center">
                <small>Copyright &copy; 2020 Greve x Boleto - Todos os Direitos Reservados.</small>
            </div>
        </footer>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../../../js/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="../../../js/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="../../../js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
