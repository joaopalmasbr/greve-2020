<!doctype html>
<html lang="pt">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" href="../conteudo/images/favicon/ativo.png" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="../css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/financeiro.css">
        <title>GxB</title>
        <style>
            #sticky-footer {
                flex-shrink: none;
            }
        </style>
    </head>
    <body>
    <div class="topo">
        <div class="text-center">
            <img src="../conteudo/images/icon/ativo.png" id="logoIndex">
        </div>

        <div class="text-center">
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="bNav">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="./">Início<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./inscricoes">Inscrições</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./atleticas">Atléticas</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./modalidades">Modalidades</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./instituicoes">Instituições</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="./cursos">Cursos</a>
                        </li>

                </div>
            </nav>
        </div>
    </div>
        <br>
        <?php
            try {
                include("../connect.php");
            } catch (Exception $e) {
                echo "<script>window.location.assign('../')</script>";
            }

            include("conteudo.php");
        ?>
        <footer id="sticky-footer" class="py-4 bg-dark text-white-50">
            <div class="container text-center">
                <small>Copyright &copy; 2020 Greve x Boleto - Todos os Direitos Reservados.</small>
            </div>
        </footer>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="../js/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="../js/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="../js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
