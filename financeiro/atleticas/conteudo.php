<div class="container">
    <a class="btn btn-block btn-primary" href="./adicionar/">Adicionar</a>
    <br>
    <div class="table-responsive">
        <table class="table table-sm table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Curso</th>
                    <th scope="col">Instituicao</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <?php
            $query = "SELECT * FROM `atletica`";
            if ($result = $mysqli->query($query)) {
                while ($row = $result->fetch_assoc()) {
                    $id = $row['id'];
                    $nome = $row['nome'];
                    $curso = $row['curso'];
                    $instituicao = $row['instituicao'];
                    $query1 = "SELECT * FROM `curso` where id=$curso";
                    if ($result1 = $mysqli->query($query1)) {
                        while ($row1 = $result1->fetch_assoc()) {
                            $curso = $row1['nome'];
                        }
                    }
                    $query2 = "SELECT * FROM `instituicao` where id=$instituicao";
                    if ($result2 = $mysqli->query($query2)) {
                        while ($row2 = $result2->fetch_assoc()) {
                            $instituicao = $row2['nome'];
                        }
                    }
                    echo "<tr>
                        <th scope=\"row\">$id</th>                  
                            <td>$nome</td>
                            <td>$curso</td>
                            <td>$instituicao</td>
                            <td><a class=\"btn btn-sm btn-warning\" href=\"./editar/?id=$id\"><img src='../../img/edit.png' height='15'></a> 
                            <a class=\"btn btn-sm btn-danger\" href=\"./deletar/?id=$id\"><img src='../../img/delete.png' height='15'></a></td>
                        </tr>";
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
