<?php
include("../../../connect.php");
$id = $_GET['id'];
$query = "SELECT * FROM `atletica` where id=$id;";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
        $nome = $row['nome'];
        $curso = $row['curso'];
        $instituicao = $row['instituicao'];
//        print_r($row);
    }
}
?>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Atleticas</a></li>
            <li class="breadcrumb-item active" saria-current="page">Editar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" value="<?php echo $nome; ?>" id="nome" name="nome" placeholder="Nome da Atletica" autofocus required>
        </div>
        <div class="form-group">
            <label for="curso">Curso</label>
            <select class="form-control" id="curso" name="curso" required>
                <?php
                $query1 = "SELECT * FROM `curso`";
                if ($result1 = $mysqli->query($query1)) {
                    while ($row1 = $result1->fetch_assoc()) {
                        $id1 = $row1['id'];
                        $nome1 = $row1['nome'];
                        foreach ($result1 as $linha){
    //                        echo "<option value=\"$id\">$nome</option>";
                            if ($linha['id']==$curso){
                                echo "<option value='"; print_r($linha['id']); echo "' selected>"; print_r($linha['nome']);echo "</option>";
                            } else {
                                echo "<option value='"; print_r($linha['id']); echo "'>"; print_r($linha['nome']);echo "</option>";
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="instituicao">Instituicao</label>
            <select class="form-control" id="instituicao" name="instituicao" required>
                <?php
                $query2 = "SELECT * FROM `instituicao`";
                if ($result2 = $mysqli->query($query2)) {
                    while ($row2 = $result2->fetch_assoc()) {
                        $id2 = $row2['id'];
                        $nome2 = $row2['nome'];
    //                    echo "<option value=\"$id\">$nome</option>";
                        foreach ($result2 as $linha2){
    //                        echo "<option value=\"$id\">$nome</option>";
                            if ($linha2['id']==$instituicao){
                                echo "<option value='"; print_r($linha2['id']); echo "' selected>"; print_r($linha2['nome']);echo "</option>";
                            } else {
                                echo "<option value='"; print_r($linha2['id']); echo "'>"; print_r($linha2['nome']);echo "</option>";
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
        <input value="<?php echo $id; ?>" name="id" id="id" hidden>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </form>
</div>
