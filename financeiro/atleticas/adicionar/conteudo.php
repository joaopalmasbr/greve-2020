<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Atleticas</a></li>
            <li class="breadcrumb-item active" saria-current="page">Adicionar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome da instituicao" autofocus required>
        </div>
        <div class="form-group">
            <label for="curso">Curso</label>
            <select class="form-control" id="curso" name="curso" required>
                <?php
                $query1 = "SELECT * FROM `curso`";
                if ($result1 = $mysqli->query($query1)) {
                    while ($row1 = $result1->fetch_assoc()) {
                        $id = $row1['id'];
                        $nome = $row1['nome'];
                        echo "<option value=\"$id\">$nome</option>";
                    }
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="instituicao">Instituicao</label>
            <select class="form-control" id="instituicao" name="instituicao" required>
                <?php
                $query2 = "SELECT * FROM `instituicao`";
                if ($result2 = $mysqli->query($query2)) {
                    while ($row2 = $result2->fetch_assoc()) {
                        $id = $row2['id'];
                        $nome = $row2['nome'];
                        echo "<option value=\"$id\">$nome</option>";
                    }
                }
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </form>
</div>
