<?php
include("../../../connect.php");
$id = $_GET['id'];
$query = "SELECT * FROM `instituicao` where id=$id;";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
        $nome = $row['nome'];
        $tipo = $row['tipo'];
        $cidade = $row['cidade'];
        $estado = $row['estado'];
//        print_r($row);
    }
}
?>
<div class="container">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Instituicoes</a></li>
            <li class="breadcrumb-item active" saria-current="page">Editar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" value="<?php echo $nome; ?>" class="form-control" id="nome" name="nome" placeholder="Nome da instituicao" autofocus required>
        </div>
        <div class="form-group">
            <label for="tipo">Tipo</label>
            <select class="form-control" id="tipo" name="tipo" required>
                <?php
                if ($tipo == 0) {
                    echo "<option value=\"0\" selected>Boleto</option>";
                    echo "<option value=\"1\">Greve</option>";
                } else {
                    echo "<option value=\"0\" >Boleto</option>";
                    echo "<option value=\"1\" selected>Greve</option>";
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label for="estado">Estado</label>
            <select class="form-control" id="estado" name="estado" required>
                <option value="AC" <?php if ($estado=="AC"){echo " selected ";} ?>>Acre</option>
                <option value="AL"<?php if ($estado=="AL"){echo " selected ";} ?>>Alagoas</option>
                <option value="AM"<?php if ($estado=="AM"){echo " selected ";} ?>>Amazonas</option>
                <option value="AP"<?php if ($estado=="AP"){echo " selected ";} ?>>Amapá</option>
                <option value="BA"<?php if ($estado=="BA"){echo " selected ";} ?> >Bahia</option>
                <option value="CE"<?php if ($estado=="CE"){echo " selected ";} ?>>Ceará</option>
                <option value="DF"<?php if ($estado=="DF"){echo " selected ";} ?>>Distrito Federal</option>
                <option value="ES"<?php if ($estado=="ES"){echo " selected ";} ?>>Espírito Santo</option>
                <option value="GO"<?php if ($estado=="GO"){echo " selected ";} ?>>Goiás</option>
                <option value="MA"<?php if ($estado=="MA"){echo " selected ";} ?>>Maranhão</option>
                <option value="MG"<?php if ($estado=="MG"){echo " selected ";} ?>>Minas Gerais</option>
                <option value="MS"<?php if ($estado=="MS"){echo " selected ";} ?>>Mato Grosso do Sul</option>
                <option value="MT"<?php if ($estado=="MT"){echo " selected ";} ?>>Mato Grosso</option>
                <option value="PA"<?php if ($estado=="PA"){echo " selected ";} ?>>Pará</option>
                <option value="PB"<?php if ($estado=="PB"){echo " selected ";} ?>>Paraíba</option>
                <option value="PE"<?php if ($estado=="PE"){echo " selected ";} ?>>Pernambuco</option>
                <option value="PI"<?php if ($estado=="PI"){echo " selected ";} ?>>Piauí</option>
                <option value="PR"<?php if ($estado=="PR"){echo " selected ";} ?>>Paraná</option>
                <option value="RJ"<?php if ($estado=="RJ"){echo " selected ";} ?>>Rio de Janeiro</option>
                <option value="RN"<?php if ($estado=="RN"){echo " selected ";} ?>>Rio Grande do Norte</option>
                <option value="RO"<?php if ($estado=="RO"){echo " selected ";} ?>>Rondônia</option>
                <option value="RR"<?php if ($estado=="RR"){echo " selected ";} ?>>Roraima</option>
                <option value="RS"<?php if ($estado=="RS"){echo " selected ";} ?>>Rio Grande do Sul</option>
                <option value="SC"<?php if ($estado=="SC"){echo " selected ";} ?>>Santa Catarina</option>
                <option value="SE"<?php if ($estado=="SE"){echo " selected ";} ?>>Sergipe</option>
                <option value="SP"<?php if ($estado=="SP"){echo " selected ";} ?>>São Paulo</option>
                <option value="TO"<?php if ($estado=="TO"){echo " selected ";} ?>>Tocantins</option>
            </select>
        </div>
        <div class="form-group">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" VALUE="<?php echo $cidade; ?>" id="cidade" name="cidade" placeholder="Cidade" required>
        </div>
        <input value="<?php echo $id; ?>" name="id" id="id" hidden>
        <button type="submit" class="btn btn-block btn-primary">Salvar</button>
        <br>
    </form>
</div>
