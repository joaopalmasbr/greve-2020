<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Modalidades</a></li>
            <li class="breadcrumb-item active" saria-current="page">Adicionar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="nome" placeholder="Nome da Modalidade" autofocus required>
        </div>
        <div class="form-group">
            <label for="atletas">Atletas</label>
            <input type="number" class="form-control" id="atletas" name="atletas" placeholder="Quantidade de Atletas" required>
        </div>
        <div class="form-group">
            <label for="valor">Valor</label>
            <input type="number" class="form-control" id="valor" name="valor" placeholder="Valor" required>
        </div>
        <div class="form-group">
            <label for="alimentos">Valor</label>
            <input type="number" class="form-control" id="alimentos" name="alimentos" placeholder="Alimentos em KG" required>
        </div>
        <div class="form-group">
            <label for="vagas">Vagas</label>
            <input type="number" class="form-control" id="vagas" name="vagas" placeholder="Numero de Vagas" required>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
        <br>
    </form>
</div>
