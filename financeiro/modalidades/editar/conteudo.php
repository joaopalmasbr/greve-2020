<?php
$id = $_GET['id'];
$query = "SELECT * FROM `modalidades` where id=$id;";
if ($result = $mysqli->query($query)) {
    while ($row = $result->fetch_assoc()) {
        $id = $row['id'];
        $nome = $row['nome'];
        $atletas = $row['atletas'];
        $valor = $row['valor'];
        $alimentos = $row['alimentos'];
        $vagas = $row['vagas'];
    }
}
?>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../">Modalidades</a></li>
            <li class="breadcrumb-item active" saria-current="page">Editar</a></li>
        </ol>
    </nav>
    <form method="post" autocomplete="off" action="action.php">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" value="<?php echo $nome; ?>" id="nome" name="nome" placeholder="Nome da Modalidade" autofocus required>
        </div>
        <div class="form-group">
            <label for="atletas">Atletas</label>
            <input type="number" class="form-control" value="<?php echo $atletas; ?>" id="atletas" name="atletas" placeholder="Quantidade de Atletas" required>
        </div>
        <div class="form-group">
            <label for="valor">Valor</label>
            <input type="number" class="form-control" value="<?php echo $valor; ?>" id="valor" name="valor" placeholder="Valor" required>
        </div>
        <div class="form-group">
            <label for="alimentos">Alimentos</label>
            <input type="number" class="form-control" value="<?php echo $alimentos; ?>" id="alimentos" name="alimentos" placeholder="Alimentos em KG" required>
        </div>
        <div class="form-group">
            <label for="vagas">Vagas</label>
            <input type="number" class="form-control" value="<?php echo $vagas; ?>" id="vagas" name="vagas" placeholder="Numero de Vagas" required>
        </div>
        <input value="<?php echo $id; ?>" name="id" id="id" hidden>
        <button type="submit" class="btn btn-primary btn-block">Salvar</button>
        <br>
    </form>
</div>
