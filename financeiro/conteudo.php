<div class="container" id="boxContentOption">
    <div class="row row-cols-1 row-cols-md-2">
        <div class="col mb-4">
            <div class="card text-black" id="cardEdit">
                <div class="card-header">Atletas</div>
                <div class="card-body">
                    <h5 class="card-title"><?php
                        // include("../../connect.php");
                        $query1 = "SELECT count(*) as cont FROM `atleta`";
                        if ($result1 = $mysqli->query($query1)) {
                            while ($row1 = $result1->fetch_assoc()) {
                                $contatletas = $row1['cont'];
                                echo $contatletas;
                            }
                        }
                        ?></h5>
                    <a href="./atletas" class="btn btn-outline-dark">Vizualizar</a>
                </div>
            </div>
        </div>
        <div class="col mb-4">
            <div class="card text-black" id="cardEdit">
                <div class="card-header">Atléticas</div>
                <div class="card-body">
                    <h5 class="card-title"><?php
                        // include("../../connect.php");
                        $query2 = "SELECT count(*) as cont FROM `atletica`";
                        if ($result2 = $mysqli->query($query2)) {
                            while ($row2 = $result2->fetch_assoc()) {
                                $contatleticas = $row2['cont'];
                                echo $contatleticas;
                            }
                        }
                        ?></h5>
                    <a href="./atleticas" class="btn btn-outline-dark">Vizualizar</a>
                </div>
            </div>
        </div>
        <div class="col mb-4">
            <div class="card text-black" id="cardEdit">
                <div class="card-header">Modalidades</div>
                <div class="card-body">
                    <h5 class="card-title">
                        <?php
                        // include("../../connect.php");
                        $query3 = "SELECT count(*) as cont FROM `modalidades`";
                        if ($result3 = $mysqli->query($query3)) {
                            while ($row3 = $result3->fetch_assoc()) {
                                $contmodalidades = $row3['cont'];
                                echo $contmodalidades;
                            }
                        }
                        ?>
                    </h5>
                    <a href="./modalidades" class="btn btn-outline-dark">Vizualizar</a>
                </div>
            </div>
        </div>
        <div class="col mb-4">
            <div class="card text-black" id="cardEdit">
                <div class="card-header">Instituições</div>
                <div class="card-body">
                    <h5 class="card-title">
                        <?php
                        // include("../../connect.php");
                        $query4 = "SELECT count(*) as cont FROM `instituicao`";
                        if ($result4 = $mysqli->query($query4)) {
                            while ($row4 = $result4->fetch_assoc()) {
                                $continstituicoes = $row4['cont'];
                                echo $continstituicoes;
                            }
                        }
                        ?>
                    </h5>
                    <a href="./instituicoes" class="btn btn-outline-dark">Vizualizar</a>
                </div>
            </div>
        </div>
    </div>
</div>
