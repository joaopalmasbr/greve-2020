<div class="container-fluid">
    <div class="row no-gutter">
        <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div>
        <div class="col-md-8 col-lg-6">
            <div class="login d-flex align-items-center py-5">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9 col-lg-8 mx-auto">
                            <h3 class="login-heading mb-4 text-light">Bem vindo!</h3>
                            <form method="post" autocomplete="off" action="./action.php" target="_self">
                                <div class="form-label-group">
                                    <input type="text" id="inputusuario" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
                                    <label for="inputusuario">Usuario</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="password" id="inputsenha" name="senha" class="form-control" placeholder="Senha" required>
                                    <label for="inputsenha">Senha</label>
                                </div>
                                <div id="erro" style="display: none;">
                                    <div class="alert alert-warning" role="alert">
                                        Usuario ou senha incorretos. Tente novamente
                                    </div>
                                </div>

                                <button class="btn btn-lg btn-outline-light btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit">Entrar</button>
                                <div class="text-center">
                                    <a class="small" style="color: white !important;" href="#">Esqueceu sua senha?</a></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
